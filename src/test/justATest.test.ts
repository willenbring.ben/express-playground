import app from "../index";
import request from "supertest";

const server = app.listen("3000");

afterAll(done => {
  server.close(done);
});
const benStinktViel = () => {
  return true;
};

describe("Root-Route", () => {
  it("receives a HTTP-GET-Request", done => {
    const expected = {
      whiteboard: "This is a whiteboard"
    };
    request(server)
      .get("/")
      .expect("Content-Type", /json/)
      .expect(200, expected)
      .end(done);
  });
});
