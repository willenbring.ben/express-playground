import app from "./index";

const server = app.listen("3002", () =>
  console.log("Server is running on port 3002")
);

export default server;
