import express from "express";
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.json());
app.use((req: express.Request, res: express.Response, next: Function) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  next();
});

// GET-Request on "/" delivers a pre-configured JSON
app.get("/", (req: express.Request, res: express.Response) => {
  const myJSObject = {
    whiteboard: "This is a whiteboard"
  };
  res.json(myJSObject);
});

export default app;
